#### Zig Raycaster

A simple raycast renderer writen in zig made for fun

![](https://gitlab.com/SamuelDeboni/zig-raycaster/-/raw/master/Screenshot.png)

- wasd to move the player
- left and right arrows to turn the camera

##### TODO
- [x] Texture mapping

for more information on the language go to https://ziglang.org/
