pub const c = @cImport({
    @cInclude("SDL2/SDL.h");
});

const std = @import("std");
const math = std.math;
const assert = std.debug.assert;
const print = std.debug.print;

pub const win_width = 1024;
pub const win_height = 384;
var screen_buffer = [_]u8{0} ** (win_width * win_height * 3);

// Inputs
pub var up_key = false;
pub var left_key = false;
pub var right_key = false;
pub var down_key = false;

pub var w_key = false;
pub var a_key = false;
pub var s_key = false;
pub var d_key = false;

const BmpHeader = packed struct {
    file_type: u16,
    file_size: u32,
    reserved1: u16,
    reserved2: u16,
    bitmap_offset: u32,

    size: u32,
    width: i32,
    height: i32,
    planes: u16,
    bits_per_pixel: u16,

    compression: u32,
    size_of_bitmap: u32,
    horz_resolution: i32,
    vert_resolution: i32,
    colors_used: u32,
    colors_important: u32,
};

pub const Texture = struct {
    width: u32,
    height: u32,
    pitch: u32 = 0,
    raw: []const u8,
};

// Simple texture mapping function, no filter
pub inline fn textureMap(u: f32, v: f32, tex: Texture) Color {
    const tex_u = @floatToInt(u32, (@intToFloat(f32, tex.width) * u));
    const tex_v = @floatToInt(u32, (@intToFloat(f32, tex.height) * v));
    const pixel = tex.raw[(tex_u + tex_v * tex.height) * 4 ..][0..4];
    return Color{
        .r = @intToFloat(f32, pixel[2]) / 255.0,
        .g = @intToFloat(f32, pixel[1]) / 255.0,
        .b = @intToFloat(f32, pixel[0]) / 255.0,
        .a = @intToFloat(f32, pixel[3]) / 255.0,
    };
}

pub fn colorFromRgba(r: u8, g: u8, b: u8, a: u8) Color {
    return Color{
        .r = @intToFloat(f32, r) / 255.0,
        .g = @intToFloat(f32, g) / 255.0,
        .b = @intToFloat(f32, b) / 255.0,
        .a = @intToFloat(f32, a) / 255.0,
    };
}

/// This function is problably very unsafe
/// it will change later
pub fn textureFromBmpData(bmp_data: []const u8) !Texture {
    const header: *const BmpHeader = @ptrCast(
        *const BmpHeader,
        bmp_data[0..@sizeOf(BmpHeader)],
    );

    if (header.file_type != 0x4d42) return error.NotBmpFile;
    if (header.compression != 3) return error.CompressedFile;
    if (header.bits_per_pixel != 32) return error.InvalidBitsPerPixel;

    var result_image: Texture = undefined;
    result_image.width = @intCast(u32, header.width);
    result_image.height = @intCast(u32, header.height);
    result_image.pitch = 0;
    result_image.raw = bmp_data[header.bitmap_offset..];

    return result_image;
}

pub inline fn loadBMP(path: []const u8) !Texture {
    const data = try std.fs.cwd().readFileAlloc(std.heap.c_allocator, path, 1024 * 1024 * 128);
    return textureFromBmpData(data);
}

const TGAHeader = packed struct {
    id_lenth: u8,
    colour_map_type: u8,
    data_type_code: u8,
    color_map_origin: u16,
    color_map_length: u16,
    color_map_depth: u8,
    x_origin: u16,
    y_origin: u16,
    width: u16,
    height: u16,
    bits_per_pixel: u8,
    image_descriptor: u8,
};

/// A simple function that loads a simple Runlength encoded RGBA TGA image
pub fn loadTGA(path: []const u8) !Texture {
    const file_data = try std.fs.cwd().readFileAlloc(std.heap.c_allocator, path, 1024 * 1024 * 128);
    defer std.heap.c_allocator.free(file_data);
    const header = @ptrCast(*TGAHeader, &file_data[0]);

    // Assert that the image is Runlength encoded RGB
    if (header.data_type_code != 10) {
        return error.InvalidTGAFormat;
    }

    if (header.bits_per_pixel != 32) {
        return error.InvalidBitsPerPixel;
    }

    var data = file_data[(@sizeOf(TGAHeader) + header.id_lenth)..][0..(file_data.len - 26)];

    var result = Texture{
        .width = header.width,
        .height = header.height,
        .raw = undefined,
    };
    var result_data = try std.heap.c_allocator.alloc(u8, header.width * header.height * 4);
    for (result_data) |*rd| rd.* = 0;
    errdefer std.heap.c_allocator.free(result.raw);

    var index: usize = 0;
    var texture_index: usize = 0;
    outer_loop: while (index < data.len) {
        const pb = data[index];
        index += 1;
        const packet_len = pb & 0x7f;

        if ((pb & 0x80) == 0x00) { // raw packet
            var i: usize = 0;
            while (i <= packet_len) : (i += 1) {
                result_data[texture_index] = data[index];
                result_data[texture_index + 1] = data[index + 1];
                result_data[texture_index + 2] = data[index + 2];
                result_data[texture_index + 3] = data[index + 3];
                texture_index += 4;
                if (texture_index >= result_data.len - 3) break :outer_loop;
                index += 4;
            }
        } else { // rl packet
            var i: usize = 0;
            while (i <= packet_len) : (i += 1) {
                result_data[texture_index] = data[index];
                result_data[texture_index + 1] = data[index + 1];
                result_data[texture_index + 2] = data[index + 2];
                result_data[texture_index + 3] = data[index + 3];
                texture_index += 4;
                if (texture_index >= result_data.len - 3) break :outer_loop;
            }
            index += 4;
        }
    }

    result.raw = result_data;
    return result;
}

test "TGA_Read" {
    assert(@sizeOf(TGAHeader) == 18);
    _ = try loadTGA("potato.tga");
}

/// Initialize SDL and start the event loop
pub fn init(
    update_fn: fn (delta: f32) anyerror!void,
    start_fn: fn () anyerror!void,
) !void {
    assert(c.SDL_Init(c.SDL_INIT_EVERYTHING) == 0);
    _ = c.SDL_SetHint(c.SDL_HINT_RENDER_SCALE_QUALITY, "2");

    const window: *c.SDL_Window = c.SDL_CreateWindow(
        "Raycaster",
        c.SDL_WINDOWPOS_CENTERED,
        c.SDL_WINDOWPOS_CENTERED,
        win_width,
        win_height,
        0,
    ) orelse unreachable;

    const renderer: *c.SDL_Renderer = c.SDL_CreateRenderer(
        window,
        -1,
        c.SDL_RENDERER_ACCELERATED,
    ) orelse unreachable;
    try start_fn();
    try mainLoop(window, renderer, update_fn);
}

/// Main event loop, calls the update_fn function
fn mainLoop(
    window: var,
    renderer: var,
    update_fn: fn (delta: f32) anyerror!void,
) !void {
    var should_quit = false;
    var event: c.SDL_Event = undefined;
    var delta: f32 = 0;

    while (!should_quit) {
        var initTime: u32 = c.SDL_GetTicks();

        _ = c.SDL_PollEvent(&event);

        switch (event.type) {
            c.SDL_QUIT => should_quit = true,

            c.SDL_KEYDOWN => {
                switch (event.key.keysym.sym) {
                    c.SDLK_UP => up_key = true,
                    c.SDLK_LEFT => left_key = true,
                    c.SDLK_RIGHT => right_key = true,
                    c.SDLK_DOWN => down_key = true,

                    c.SDLK_w => w_key = true,
                    c.SDLK_a => a_key = true,
                    c.SDLK_s => s_key = true,
                    c.SDLK_d => d_key = true,

                    else => {},
                }
            },

            c.SDL_KEYUP => {
                switch (event.key.keysym.sym) {
                    c.SDLK_UP => up_key = false,
                    c.SDLK_LEFT => left_key = false,
                    c.SDLK_RIGHT => right_key = false,
                    c.SDLK_DOWN => down_key = false,

                    c.SDLK_w => w_key = false,
                    c.SDLK_a => a_key = false,
                    c.SDLK_s => s_key = false,
                    c.SDLK_d => d_key = false,
                    else => {},
                }
            },

            else => {},
        }

        _ = c.SDL_RenderClear(renderer);
        try update_fn(delta);

        var screen_surface = c.SDL_CreateRGBSurfaceFrom(
            &screen_buffer,
            win_width,
            win_height,
            24,
            3 * win_width,
            0x00ff0000,
            0x0000ff00,
            0x000000ff,
            0x00000000,
        );
        {
            var texture = c.SDL_CreateTextureFromSurface(renderer, screen_surface);
            _ = c.SDL_RenderCopy(renderer, texture, null, null);
            _ = c.SDL_RenderPresent(renderer);
            c.SDL_DestroyTexture(texture);
        }

        initTime = c.SDL_GetTicks() - initTime;
        delta = @intToFloat(f32, initTime) / 1000;
    }
}

pub const Color = struct {
    r: f32,
    g: f32,
    b: f32,
    a: f32,
};

pub inline fn colorc(r: f32, g: f32, b: f32, a: f32) Color {
    return Color{ .r = r, .g = g, .b = b, .a = a };
}

pub inline fn putPixel(x: u32, y: u32, color: Color) void {
    if (x > win_width - 1) return;
    if (y > win_height - 1) return;
    if (x < 0) return;
    if (y < 0) return;

    const pixel = screen_buffer[(x + y * win_width) * 3 ..][0..3];
    // TODO: Alpha blending
    if (color.a > 0.999) {
        pixel[0] = @floatToInt(u8, color.b * 255);
        pixel[1] = @floatToInt(u8, color.g * 255);
        pixel[2] = @floatToInt(u8, color.r * 255);
    }
}

/// draw a filled rectangle
pub fn fillRect(x: u32, y: u32, w: u32, h: u32, color: Color) void {
    if (x > win_width) return;
    if (y > win_height) return;

    const width = if (x + w < win_width) w else win_width - x;
    const height = if (y + h < win_height) h else win_height - y;

    var y_i: u32 = y;
    while (y_i < h + y) : (y_i += 1) {
        var x_i: u32 = x;
        while (x_i < w + x) : (x_i += 1) {
            putPixel(x_i, y_i, color);
        }
    }
}

pub fn drawTexture(x: u32, y: u32, w: u32, h: u32, tex: Texture) void {
    if (x > win_width) return;
    if (y > win_height) return;

    const width = if (x + w < win_width) w else win_width - x;
    const height = if (y + h < win_height) h else win_height - y;

    var y_i: u32 = 0;
    while (y_i < h) : (y_i += 1) {
        var x_i: u32 = 0;
        while (x_i < w) : (x_i += 1) {
            const pixel = screen_buffer[((x_i + x) + (y_i + y) * win_width) * 3 ..][0..3];
            const tex_pixel_pos = (((x_i * tex.width) / w) + ((y_i * tex.height) / h) * tex.width) * 4;
            const tex_pixel = tex.raw[tex_pixel_pos..][0..4];

            pixel[0] = tex_pixel[0];
            pixel[1] = tex_pixel[1];
            pixel[2] = tex_pixel[2];
        }
    }
}

/// draw a hollow rectangle
pub fn drawRect(x: u32, y: u32, w: u32, h: u32, color: Color) void {
    if (x >= win_width) return;
    if (y >= win_height) return;

    const width = if (x + w < win_width) w else win_width - x;
    const height = if (y + h < win_height) h else win_height - y;

    var xi: u32 = 0;
    while (xi < x + w) : (xi += 1) {
        putPixel(xi, y, color);
        putPixel(xi, y + h - 1, color);
    }

    var yi: u32 = 1;
    while (yi <= y + h) : (yi += 1) {
        putPixel(x, yi, color);
        putPixel(x + w - 1, yi, color);
    }
}

/// Draw a line from [xa, ya] to [xa, yb]
pub fn drawLine(xa: i32, ya: i32, xb: i32, yb: i32, color: Color) void {
    var offset_x: i32 = 0;
    var offset_y: i32 = 0;
    var dx: i32 = 0;
    var dy: i32 = 0;

    var px_mult: i32 = 1;
    var py_mult: i32 = 1;

    if (xa > xb) {
        offset_x = xb;
        dx = xa - xb;
        if (ya < yb) {
            px_mult = -1;
            offset_x = xa;
        }
    } else {
        offset_x = xa;
        dx = xb - xa;
    }

    if (ya > yb) {
        offset_y = yb;
        dy = ya - yb;
        if (xa < xb) {
            py_mult = -1;
            offset_y = ya;
        }
    } else {
        offset_y = ya;
        dy = yb - ya;
    }

    var px: i32 = 0;
    var py: i32 = 0;

    var x: i32 = 0;
    var y: i32 = 0;
    while (px < dx or py < dy) {
        const pixel_x = px * px_mult + offset_x;
        const pixel_y = py * py_mult + offset_y;
        if (pixel_x >= 0 and pixel_y >= 0)
            putPixel(@intCast(u32, pixel_x), @intCast(u32, pixel_y), color);

        var a = x + dy;
        var b = y + dx;

        var d1 = i32Abs(a - y - dx);
        var d2 = i32Abs(x - b - dx);

        if (d2 < d1) {
            py += 1;
            y = b;
        } else {
            px += 1;
            x = a;
        }

        if (px > win_width or py > win_height or (py > dy and dx < dy) or (px > dx and dy < dx)) break;
    }
}

pub fn fillScreenWithRGBColor(r: u8, g: u8, b: u8) void {
    var index: usize = 0;
    while (index < screen_buffer.len) : (index += 3) {
        screen_buffer[index] = b;
        screen_buffer[index + 1] = g;
        screen_buffer[index + 2] = r;
    }
}

pub inline fn i32Abs(n: i32) i32 {
    if (n < 0) return -n;
    return n;
}

pub inline fn f32Frac(n: f32) f32 {
    return n - @trunc(n);
}
