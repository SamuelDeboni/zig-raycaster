const draw = @import("pixel_draw.zig");
const c = draw.c;
const Color = draw.Color;
const win_width = draw.win_width;
const win_height = draw.win_height;

const std = @import("std");
const math = std.math;
const assert = std.debug.assert;
const print = std.debug.print;

pub fn main() anyerror!void {
    // This call will start the update loop until you close
    // the window
    try draw.init(update, start);
}

var texture_table: [5]draw.Texture = undefined;
var potato_tex: draw.Texture = undefined;
pub fn start() anyerror!void {
    texture_table[0] = try draw.loadBMP("potato.bmp");
    texture_table[1] = try draw.loadBMP("brick.bmp");
    texture_table[2] = try draw.loadTGA("potato.tga");
    texture_table[3] = try draw.loadBMP("potato.bmp");
    texture_table[4] = try draw.loadBMP("potato.bmp");
}

// values from 0 to 4 represent the colors from the map_colors,
// values from 5 to 9 represent the textures from the texture table
const map = [_][16]u8{
    [_]u8{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 4, 4, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 7, 6, 0, 0, 0, 0, 7, 7, 7, 7, 0, 6, 7, 7, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 0, 7, 7, 7, 0, 7, 7, 7, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 0, 0, 0, 0, 0, 0, 2, 4, 0, 0, 0, 0, 0, 0, 6 },
    [_]u8{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 },
};
const map_colors = [_]Color{
    draw.colorc(0, 0, 0, 1),
    draw.colorc(0, 0, 1, 1),
    draw.colorc(0, 1, 0, 1),
    draw.colorc(0, 1, 1, 1),
    draw.colorc(1, 0, 0, 1),
};

const pi: f32 = 3.1415926535;
const tau: f32 = pi * 2.0;

var player_pos_x: f32 = 1.5;
var player_pos_y: f32 = 1.5;
var player_dir: f32 = 0.0;
var ray_size: f32 = 10.0;

fn update(delta: f32) anyerror!void {
    //print("{d:4.0}\t\t{d:4.4}\n", .{ 1.0 / delta, delta * 1000 });

    // draw floor and sky
    draw.fillScreenWithRGBColor(55, 55, 150);
    draw.fillRect(
        win_width / 2,
        win_height / 2,
        win_width / 2,
        win_height / 2 + 2,
        draw.colorc(0.2, 0.2, 0.2, 1),
    );

    // Draw map
    for (map) |line, y| {
        for (line) |tile, x| {
            if (tile > 4) { // draw texture
                draw.drawTexture(@intCast(u32, x * 32), @intCast(u32, y * 32), 32, 32, texture_table[tile - 5]);
            } else { // draw solid color
                draw.fillRect(@intCast(u32, x * 32), @intCast(u32, y * 32), 32, 32, map_colors[tile]);
                draw.drawRect(@intCast(u32, x * 32), @intCast(u32, y * 32), 32, 32, map_colors[0]);
            }
        }
    }

    // player movment
    var next_player_pos_x: f32 = player_pos_x;
    var next_player_pos_y: f32 = player_pos_y;

    if (draw.s_key) {
        next_player_pos_y -= delta * 3.0 * math.sin(player_dir);
        next_player_pos_x -= delta * 3.0 * math.cos(player_dir);
    }
    if (draw.w_key) {
        next_player_pos_y += delta * 3.0 * math.sin(player_dir);
        next_player_pos_x += delta * 3.0 * math.cos(player_dir);
    }
    if (draw.a_key) {
        next_player_pos_y -= delta * 3.0 * math.sin(player_dir + pi * 0.5);
        next_player_pos_x -= delta * 3.0 * math.cos(player_dir + pi * 0.5);
    }
    if (draw.d_key) {
        next_player_pos_y += delta * 3.0 * math.sin(player_dir + pi * 0.5);
        next_player_pos_x += delta * 3.0 * math.cos(player_dir + pi * 0.5);
    }

    if (map[@floatToInt(u32, next_player_pos_y)][@floatToInt(u32, next_player_pos_x)] == 0) {
        player_pos_x = next_player_pos_x;
        player_pos_y = next_player_pos_y;
    }

    if (draw.left_key) player_dir -= delta * 2;
    if (draw.right_key) player_dir += delta * 2;

    if (player_dir > tau) player_dir -= tau;
    if (player_dir < 0.0) player_dir += tau;

    const player_screen_pos_x = @floatToInt(i32, player_pos_x * @intToFloat(f32, win_width / 2) / 16);
    const player_screen_pos_y = @floatToInt(i32, player_pos_y * @intToFloat(f32, win_height) / 12);

    // draw player
    draw.fillRect(
        @intCast(u32, player_screen_pos_x - 8),
        @intCast(u32, player_screen_pos_y - 8),
        16,
        16,
        draw.colorc(0, 1, 0, 1),
    );

    const fov: f32 = 0.18;
    const angle: f32 = player_dir - pi * fov;
    renderScene(angle, fov);

    draw.drawLine(win_width / 2, 0, win_width / 2, win_height, map_colors[0]);
}

fn renderScene(_angle: f32, fov: f32) void {
    const player_screen_pos_x = @floatToInt(i32, player_pos_x * @intToFloat(f32, win_width / 2) / 16);
    const player_screen_pos_y = @floatToInt(i32, player_pos_y * @intToFloat(f32, win_height) / 12);

    var angle = _angle;
    var loop_count: u32 = 0;
    while (angle < player_dir + pi * fov) : (angle += (pi * fov * 2.0 / 512.0)) {
        var ray_angle = angle;
        if (angle > tau) ray_angle -= tau;
        if (angle < 0.0) ray_angle += tau;

        // ====================================================================
        // Cast Rays
        // ====================================================================
        const angle_tan = math.tan(ray_angle);
        const one_over_angle_tan = 1.0 / angle_tan;
        const looking_down = ray_angle > 0 and ray_angle < pi;
        const looking_right = ray_angle < pi * 0.5 or ray_angle > pi * 1.5;

        var ray_pos_x = player_pos_x;
        var ray_pos_y = player_pos_y;

        var x_step: f32 = if (looking_right) 1.0 else -1.0;
        var y_step = angle_tan * x_step;

        var hit_value_x: u8 = 0;
        var dist_check_x: f32 = 200.0;

        var ray_pos_x_check_x: f32 = if (looking_right) @trunc(ray_pos_x) + 1.0 else @trunc(ray_pos_x) - 0.001;
        var ray_pos_y_check_x: f32 = ray_pos_y + (ray_pos_x_check_x - ray_pos_x) * angle_tan;
        while (true) {
            if (ray_pos_x_check_x > 0 and ray_pos_x_check_x < 16 and ray_pos_y_check_x > 0 and ray_pos_y_check_x < 12) {
                if (map[@floatToInt(u32, ray_pos_y_check_x)][@floatToInt(u32, ray_pos_x_check_x)] != 0) {
                    const dist_x = ray_pos_x_check_x - player_pos_x;
                    const dist_y = ray_pos_y_check_x - player_pos_y;
                    dist_check_x = @sqrt(dist_x * dist_x + dist_y * dist_y);
                    hit_value_x = map[@floatToInt(u32, ray_pos_y_check_x)][@floatToInt(u32, ray_pos_x_check_x)];
                    break;
                }
            } else break;
            ray_pos_x_check_x += x_step;
            ray_pos_y_check_x += y_step;
        }

        y_step = if (looking_down) 1.0 else -1.0;
        x_step = y_step * one_over_angle_tan;

        var hit_value_y: u8 = 0;
        var dist_check_y: f32 = 200.0;

        var ray_pos_y_check_y: f32 = if (looking_down) @trunc(ray_pos_y) + 1.0 else @trunc(ray_pos_y) - 0.001;
        var ray_pos_x_check_y: f32 = ray_pos_x + (ray_pos_y_check_y - ray_pos_y) * one_over_angle_tan;
        while (true) {
            if (ray_pos_x_check_y > 0 and ray_pos_x_check_y < 16 and ray_pos_y_check_y > 0 and ray_pos_y_check_y < 12) {
                if (map[@floatToInt(u32, ray_pos_y_check_y)][@floatToInt(u32, ray_pos_x_check_y)] != 0) {
                    const dist_x = ray_pos_x_check_y - player_pos_x;
                    const dist_y = ray_pos_y_check_y - player_pos_y;
                    dist_check_y = @sqrt(dist_x * dist_x + dist_y * dist_y);
                    hit_value_y = map[@floatToInt(u32, ray_pos_y_check_y)][@floatToInt(u32, ray_pos_x_check_y)];
                    break;
                }
            } else break;
            ray_pos_y_check_y += y_step;
            ray_pos_x_check_y += x_step;
        }

        var dist: f32 = 0;
        var ray_color = draw.colorc(1, 1, 1, 1);
        var hit_on_x = true;
        var hit_value: u32 = 0;
        if (dist_check_x < dist_check_y) {
            if (hit_value_x < 5) {
                ray_color = map_colors[hit_value_x];
            }
            hit_value = hit_value_x;
            ray_color.r *= 0.6;
            ray_color.g *= 0.6;
            ray_color.b *= 0.6;

            ray_pos_x = ray_pos_x_check_x;
            ray_pos_y = ray_pos_y_check_x;
            dist = dist_check_x;
        } else {
            if (hit_value_y < 5) {
                ray_color = map_colors[hit_value_y];
            }
            hit_value = hit_value_y;
            ray_pos_x = ray_pos_x_check_y;
            ray_pos_y = ray_pos_y_check_y;
            dist = dist_check_y;
            hit_on_x = false;
        }

        // ====================================================================
        // Draw rays
        // ====================================================================
        {
            // if (@fabs(ray_pos_x - @round(ray_pos_x)) < 0.02 and
            //     @fabs(ray_pos_y - @round(ray_pos_y)) < 0.02) ray_color = map_colors[0];
            const ray_screen_pos_x = @floatToInt(i32, ray_pos_x * @intToFloat(f32, win_width / 2) / 16);
            const ray_screen_pos_y = @floatToInt(i32, ray_pos_y * @intToFloat(f32, win_height) / 12);
            draw.drawLine(player_screen_pos_x, player_screen_pos_y, ray_screen_pos_x, ray_screen_pos_y, ray_color);
        }

        // ====================================================================
        // Draw Scene
        // ====================================================================
        {
            var draw_pos_x = loop_count + win_width / 2;

            // Dot dist with player direction to avoid fisheye distortion
            dist = dist * math.cos(ray_angle - player_dir);

            var draw_h = @floatToInt(u32, 384 / dist);

            // This values is to take in to acount cliping on the texture mapping
            const tex_h = draw_h;
            var tex_h_offset: u32 = 0;

            // Height cliping
            if (draw_h > 384) {
                draw_h = 384;
                tex_h_offset = (tex_h - draw_h) / 2;
            }

            var y_index: u32 = 0;
            if (hit_value < 5) { // solid color
                while (y_index < draw_h) : (y_index += 1)
                    draw.putPixel(draw_pos_x, y_index + (192 - draw_h / 2), ray_color);
            } else { // Texture
                const u: f32 = if (hit_on_x) draw.f32Frac(ray_pos_y) else draw.f32Frac(ray_pos_x);
                while (y_index < draw_h) : (y_index += 1) {
                    const v = @intToFloat(f32, y_index + tex_h_offset) / @intToFloat(f32, tex_h);
                    var color = draw.textureMap(u, v, texture_table[hit_value - 5]);
                    color.r *= ray_color.r;
                    color.g *= ray_color.g;
                    color.b *= ray_color.b;
                    draw.putPixel(draw_pos_x, y_index + (192 - draw_h / 2), color);
                }
            }
        }

        loop_count += 1;
    }
}
