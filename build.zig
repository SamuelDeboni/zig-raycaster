const std = @import("std");
const Builder = std.build.Builder;

pub fn build(b: *Builder) void {
    var target = b.standardTargetOptions(.{});
    target.abi = .musl;

    const mode = b.standardReleaseOptions();
    const windows = b.option(bool, "windows", "create windows build") orelse false;

    var exe = b.addExecutable("raycaster", "src/main.zig");
    exe.setTarget(target);

    if (windows) {
        exe.setTarget(.{
            .cpu_arch = .x86_64,
            .os_tag = .windows,
            .abi = .gnu,
        });
    }

    exe.setBuildMode(mode);
    //exe.addIncludeDir("include");

    exe.linkSystemLibrary("c");
    if (windows) {
        exe.linkSystemLibrary("gdi32");
        exe.linkSystemLibrary("shell32");
        exe.addLibPath("lib");
    }
    exe.linkSystemLibrary("SDL2");
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
